﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class nextLevelGui : MonoBehaviour {

	public Text pts;
	public Text hp;
	public Text gp;
	public Text jp;
	public Button ok;
	// Use this for initialization
	void Start () {

		pts = GameObject.Find ("pts").GetComponent<Text>();
		pts.text = "3";
		hp = GameObject.Find ("hpPoints").GetComponent<Text>();
		gp = GameObject.Find ("gunPoints").GetComponent<Text>();
		jp = GameObject.Find ("jumpPoints").GetComponent<Text>();
		ok = GameObject.Find ("Ok").GetComponent<Button> ();
		if (PlayerPrefs.HasKey ("hpPoints"))
						hp.text = PlayerPrefs.GetInt ("hpPoints").ToString();

		if (PlayerPrefs.HasKey ("gpPoints"))
						gp.text = PlayerPrefs.GetInt ("gpPoints").ToString();

		if (PlayerPrefs.HasKey ("jpPoints"))
						jp.text = PlayerPrefs.GetInt ("jpPoints").ToString();

	}
	
	// Update is called once per frame
	public void MaisHp(){
		int i = int.Parse (pts.text);
		if (i > 0) {
			i--;
			pts.text = i.ToString ();

			i = int.Parse (hp.text);
			i++;
			hp.text = i.ToString ();
			if(int.Parse (pts.text)==0)
				ok.interactable = true;
		}

	}
	public void MenosHp(){
		int i = int.Parse (hp.text);
		if (i > 0) {
			i--;
			hp.text = i.ToString ();

			i = int.Parse (pts.text);
			i++;
			pts.text = i.ToString ();

			if(int.Parse (pts.text)>0)
				ok.interactable = false;

		}	
	}
	public void MaisGp(){
		int i = int.Parse (pts.text);
		if (i > 0) {
			i--;
			pts.text = i.ToString ();
			
			i = int.Parse (gp.text);
			i++;
			gp.text = i.ToString ();
			if(int.Parse (pts.text)==0)
				ok.interactable = true;
		}
		
	}
	public void MenosGp(){
		int i = int.Parse (gp.text);
		if (i > 0) {
			i--;
			gp.text = i.ToString ();
			
			i = int.Parse (pts.text);
			i++;
			pts.text = i.ToString ();


			if(int.Parse (pts.text)>0)
				ok.interactable = false;
			
		}	
	}
	public void MaisJp(){
		int i = int.Parse (pts.text);
		if (i > 0) {
			i--;
			pts.text = i.ToString ();
			
			i = int.Parse (jp.text);
			i++;
			jp.text = i.ToString ();
			if(int.Parse (pts.text)==0)
				ok.interactable = true;
		}
		
	}
	public void MenosJp(){
		int i = int.Parse (jp.text);
		if (i > 0) {
			i--;
			jp.text = i.ToString ();
			
			i = int.Parse (pts.text);
			i++;
			pts.text = i.ToString ();
			if(int.Parse (pts.text)>0)
				ok.interactable = false;
			
		}	
	}

	public void restaura(){
		ok.interactable = false;
		hp.text = "0";
		jp.text = "0";
		gp.text = "0";
		pts.text = "3";
	}

	public void okClick(){
		GameObject.Find ("stand").GetComponent<playerController> ().nextLevelStart(int.Parse(hp.text),  int.Parse(jp.text), int.Parse(gp.text));
		}
}
