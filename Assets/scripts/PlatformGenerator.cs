using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlatformGenerator : MonoBehaviour {

	public int countPlatforms=0;
	public int tamanhoFase = 1;

//	private Animator _animator;
	// lista com todos os fundos
	public GameObject[] avaliablePlatforms;
	public GameObject[] avaliablePlatformsFase1;
	public GameObject[] avaliablePlatformsFase2;
	public GameObject[] avaliablePlatformsFase3;
	public GameObject firstPlatform;
	// lista com os fundos criados
	public List<GameObject> currentPlatforms;
	public List<GameObject> Limiters;

	private float screenWidthInPoints;


	//teste de limitador
	private float limit;
	public GameObject limitObject;


	// Use this for initialization
	void Start () {
		if (Application.loadedLevelName == "main") {
			avaliablePlatforms = avaliablePlatformsFase1;
				}
		if (Application.loadedLevelName == "phase 1") {
			avaliablePlatforms = avaliablePlatformsFase1;
		}
		if (Application.loadedLevelName == "phase 2") {
			avaliablePlatforms = avaliablePlatformsFase2;
			currentPlatforms[0] = avaliablePlatforms[0];
		}
		if (Application.loadedLevelName == "phase 3") {
			avaliablePlatforms = avaliablePlatformsFase3;
			currentPlatforms[0] = avaliablePlatforms[0];
		}
		//_animator = this.GetComponent<Animator> ();
		limit = 0;
		firstPlatform = currentPlatforms [0];
		// pega largura da imagem
		float height = 2.0f * Camera.main.orthographicSize;
		screenWidthInPoints = height * Camera.main.aspect;
		}

	// Update is called once per frame
	void Update () {
	
	}

	// Atualizada um determinado tempo (independente do processador)
	void FixedUpdate() {
		GeneratePlatformIfRequired ();
	}

	public void restarGame(){
		countPlatforms = 0;
			List<GameObject> allPlatf = currentPlatforms;
			foreach (var platform in allPlatf) {
					//currentPlatforms.Remove (platform);
			if(platform!=firstPlatform)
					Destroy (platform);
			}
		limit = 0;
		currentPlatforms.Clear ();
		currentPlatforms.Add (firstPlatform);
		foreach (var lim in Limiters) {
				Destroy(lim);
				}
		Limiters.Clear ();
		}
	void GeneratePlatformIfRequired() {
		// inicializa lista de fundos a serem removidos
		List<GameObject> platformsToRemove = new List<GameObject> ();
		// inicializa flag que indica se e necessario adicionar fundo
		bool flagAddPlatform = true;

		// pega posicao x do personagem
		float playerX = transform.position.x;
		// pega posicao x para remover
		float removePositionPlatformX = playerX - screenWidthInPoints -10.0f;
		// pega posicao x para adicionar
		float addPositionPlatformX = playerX + screenWidthInPoints;

		//inicializa maior posicao x dos fundos
		float farthestPlatformEndX = 0;
		foreach(var platform in currentPlatforms) {
			//pega posicao central de x no fundo
			float platformWidth = platform.transform.localScale.x;
			//pega posicao mais a esquerda de x no fundo
			float platformStartX = platform.transform.position.x + 5.7f-(platformWidth * 0.5f);//- 1 - (platformWidth * 0.5f);
			//pega posicao mais a direita de x no fundo
			float platformEndX = platformStartX + platformWidth;

			//se a posiçao mais a direita for maior que a posiçao de adicionar, nao adiciona
			if (platformStartX > addPositionPlatformX) {
				flagAddPlatform = false;
			}

			//se a posicao mais a esquerda for menor que a posiçao de remover, remove
			if (platformEndX < removePositionPlatformX) {
				platformsToRemove.Add(platform);			
			}

			//pega maior posicao x dos fundos
			farthestPlatformEndX = Mathf.Max(farthestPlatformEndX,platformEndX);
		}

		foreach (var platform in platformsToRemove) {
			limit = platform.transform.position.x + platform.transform.localScale.x;
			limit = limit + platform.transform.localScale.x + platform.transform.localScale.x*0.1f - 11f;
			GameObject limitador = (GameObject)Instantiate (limitObject);
			limitador.transform.position = new Vector3 (limit,9.08f,0);
			Limiters.Add(limitador);
			currentPlatforms.Remove(platform);
			if(platform!=firstPlatform)
				Destroy (platform);
		}

		// se a flag de adicionar estiver ativada, insere
		if (flagAddPlatform) {
			addPlatform (farthestPlatformEndX);
		}
	}

	void addPlatform(float farthestPlatformEndX) {
		// escolhe um indice randomicamente ou pega a plataforma final
		int randomPlatformIndex = 0;
		if (countPlatforms > tamanhoFase) 
		{
			randomPlatformIndex = avaliablePlatforms.Length-1;
		} 
		else{
		
			randomPlatformIndex = Random.Range (0, avaliablePlatforms.Length-1);
		}
		// pega o fundo que corresponde com o indice
		GameObject platform = (GameObject)Instantiate (avaliablePlatforms [randomPlatformIndex]);
		// pega a posicao x (que e a parte mais baixa da imagem)
		float platformWidth = platform.transform.localScale.x+ 5.7f;
		// pega posicao central  do fundo
		float platformCenter = farthestPlatformEndX+platformWidth;
		// cria um novo vetor com a posicao central do fundo
		platform.transform.position = new Vector3 (platformCenter,5.1f,4.5f);
		// adiciona o novo fundo ao fundos criados
		currentPlatforms.Add (platform);
		countPlatforms++;
	}

}	
