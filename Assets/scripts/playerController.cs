﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class playerController : MonoBehaviour
{

	public int gunNumber = 0;  				// Gun in use
	public int gunEnabled=0;  				// Guns unlockeds
	private Animator _animator;    			// player animator
	public bool _facingRight = true;			// For determining which way the player is currently facing.
	public bool _flip = false;			// For determining which way the player is currently facing.
	public float damageTime = -10f;		// time of damage
	private float _yVelocity = 0f;		//
	public float _moveForce = 365f;			// Amount of force added to move the player left and right.
	public float _maxSpeed = 5f;				// The fastest the player can travel in the x axis.

	public int lives = 3;
	public Transform _groundCheck;			// A position marking where to check if the player is grounded.
	public bool _grounded = false;			// Whether or not the player is grounded.
	public bool _jump = false;				// Condition for whether the player should jump.
	public float _jumpForce = 2000f;			// Amount of force added when the player jumps.
	public Transform _gun;					//Object of gun
	public SpriteRenderer gunSprite;		//Image of gun
	public Sprite[] guns;					//Set of image of guns
	public int bombsNumber = 3;

	public int proximaFase = 2; 			//For determining which level will be next loaded

	public bool _falling = false;			// Whether or not the player is grounded.
	
	public Canvas nextLevelMenu;

	public int hpPoints = 0;
	public int jpPoints = 0;
	public int gpPoints = 0;




	void Awake()
	{
		// Setting up references.
		_groundCheck = transform.Find("_groundCheck");
		_gun = transform.Find ("Gun");
		gunSprite = GameObject.Find("gunSprite").GetComponent<SpriteRenderer>();
//		lancaBombaSprite = transform.Find("lancaBomba");


	}

	// Use this for initialization
	void Start()
	{
		Time.timeScale = 1;
		nextLevelMenu = GameObject.Find ("NextLevel").GetComponent<Canvas>();
		//Determine either load datasave or not
		if (!PlayerPrefs.HasKey ("jogoIniciado")) {
						PlayerPrefs.DeleteAll ();
						PlayerPrefs.SetInt ("jogoIniciado", 1);
		} else {
						
			lives = PlayerPrefs.GetInt ("lives");
			menu mn = GameObject.Find ("Canvas").GetComponent<menu>();
			mn.livesImg.sprite = mn.livesImgs[lives];
			this.GetComponent<PlayerHealth> ().health = PlayerPrefs.GetFloat ("hp");
			hpPoints = PlayerPrefs.GetInt("hpPoints");
			this.GetComponent<PlayerHealth> ().health += hpPoints*10;
			this.GetComponent<PlayerHealth>().UpdateHealthBar();

			
			gpPoints=PlayerPrefs.GetInt("gpPoints");
			gunNumber = PlayerPrefs.GetInt ("gunNumber");
			int gp = gpPoints /2;
			gunEnabled = PlayerPrefs.GetInt ("gunEnabled") + gp;
			if(gunEnabled>=1)
			{
				Image im = GameObject.Find ("akImage").GetComponent<Image>();
				im.color = Color.yellow;
				im.GetComponentInChildren<Text>().color = Color.white;
			}
			if(gunEnabled>=2)
			{
				Image im = GameObject.Find ("bazookaImage").GetComponent<Image>();
				im.color = Color.yellow;
				im.GetComponentInChildren<Text>().color = Color.white;
			}
			if(gunEnabled<2)
			{
				Image im = GameObject.Find ("bazookaImage").GetComponent<Image>();
				im.color = Color.gray;
				im.GetComponentInChildren<Text>().color = Color.grey;
			}
			if(gunEnabled>=1)
			{
				Image im = GameObject.Find ("akImage").GetComponent<Image>();
				im.color = Color.yellow;
				im.GetComponentInChildren<Text>().color = Color.white;
			}

			bombsNumber = PlayerPrefs.GetInt ("bombsNumber", bombsNumber);
			GameObject.Find("BombLabel").GetComponent<Text>().text = bombsNumber.ToString();;

			jpPoints =PlayerPrefs.GetInt("jpPoints");
			_jumpForce += jpPoints*300f;
			//Determine next level
			if (PlayerPrefs.HasKey ("faseSalva")) {
					proximaFase = PlayerPrefs.GetInt ("faseSalva") + 1;
			}
		}
		_animator = this.GetComponent<Animator> ();

		//load the current gun image
		gunSprite.sprite = guns [gunNumber];
	}


	// Update is called once per frame
	void Update()
	{

		// The player is grounded if a linecast to the groundcheck position hits anything on the ground layer.
		_grounded = Physics2D.Linecast(transform.position,_groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));  
		_falling = Physics2D.Linecast(transform.position,_groundCheck.position, 1 << LayerMask.NameToLayer("Deep"));  

		//Test if the player is falling and call die animation and functions
		if (_falling) {
			_animator.SetBool ("Die", true);
			GameObject.Find("colider_stand").collider2D.enabled = false;
			GameObject.Find("colider_dead").collider2D.enabled = true;

					if(lives <=0)
					{
						GameObject.Find ("Canvas").GetComponent<menu>().gameOver();
					}
					else{
						GameObject.Find ("Canvas").GetComponent<menu>().MenuDeath();
					}
				} else {

		
						// If the jump button is pressed and the player is grounded then the player should jump.
						//KeyCode.UpArrow
						if (Input.GetKeyDown (KeyCode.UpArrow) && _grounded)
								_jump = true;
						//For switch gun
						if (Input.GetKey (KeyCode.Alpha1)) {
								gunNumber = 0;
								gunSprite.sprite = guns [gunNumber];
						} else if (Input.GetKey (KeyCode.Alpha2)) {
								if(gunEnabled>=1){
									gunNumber = 1;
									gunSprite.sprite = guns [gunNumber];
								}					
						} else if (Input.GetKey (KeyCode.Alpha3)) {
								if(gunEnabled>=2){
								gunNumber = 2;
								gunSprite.sprite = guns [gunNumber];
								}
						}
				}

	}


	//For detect when the end of level and load next
	void OnTriggerEnter2D(Collider2D colisor) {
		if (colisor.gameObject.tag == "ProximaFase") {

			Time.timeScale = 0;
			nextLevelMenu.enabled = true;
			/*
			PlayerPrefs.SetInt("lives",lives);
			PlayerPrefs.SetFloat("hp", this.GetComponentInParent<PlayerHealth>().getHealth());
			PlayerPrefs.SetInt("faseSalva", proximaFase);
			PlayerPrefs.SetInt ("gunNumber", gunNumber);
			PlayerPrefs.SetInt("gunEnabled", gunEnabled);
			PlayerPrefs.SetInt("bombsNumber", bombsNumber);

			this.GetComponentInParent<PlatformGenerator>().countPlatforms = 0;
			Application.LoadLevel("phase "+proximaFase.ToString());
			*/
		}
	}

	//Inicia Nova fase
	public void nextLevelStart(int hp, int jp, int gp){

		PlayerPrefs.SetInt("lives",lives);
		PlayerPrefs.SetFloat("hp", this.GetComponentInParent<PlayerHealth>().getHealth());
		PlayerPrefs.SetInt("faseSalva", proximaFase);
		PlayerPrefs.SetInt ("gunNumber", gunNumber);
		PlayerPrefs.SetInt("gunEnabled", gunEnabled);
		PlayerPrefs.SetInt("bombsNumber", bombsNumber);
		PlayerPrefs.SetInt("hpPoints", hp);
		PlayerPrefs.SetInt("jpPoints", jp);
		PlayerPrefs.SetInt("gpPoints", gp);

		
		this.GetComponentInParent<PlatformGenerator>().countPlatforms = 0;
		if(proximaFase >3)
			Application.LoadLevel("phase 3");
		else
			Application.LoadLevel("phase "+proximaFase.ToString());
		}
	void FixedUpdate()
	{


		Vector3 _direction = new Vector2 (1.0f, 0f);
///		var _vertical = Input.GetAxis("Vertical");
		var _horizontal = Input.GetAxis("Horizontal");

		if (Time.time > damageTime + 0.5f) {

						// Cache the horizontal input.
						float _h = Input.GetAxis ("Horizontal");


						// If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...
						if (_h * rigidbody2D.velocity.x < _maxSpeed)
			// ... add a force to the player.
								rigidbody2D.AddForce (Vector2.right * _h * _moveForce);
			
		
						// If the player's horizontal velocity is greater than the maxSpeed...
						if (Mathf.Abs (rigidbody2D.velocity.x) > _maxSpeed)
			// ... set the player's velocity to the maxSpeed in the x axis.
								rigidbody2D.velocity = new Vector2 (Mathf.Sign (rigidbody2D.velocity.x) * _maxSpeed, rigidbody2D.velocity.y);



						// If the player should jump...
						if (_jump) {
								// Set the Jump animator parameter.
								_animator.SetInteger ("dir", 5);
			
								// Play a random jump audio clip.
								/*int i = Random.Range(0, jumpClips.Length);
			AudioSource.PlayClipAtPoint(jumpClips[i], transform.position);*/
			
								// Add a vertical force to the player.
								rigidbody2D.AddForce (new Vector2 (0f, _jumpForce));
			
								// Make sure the player can't jump again until the jump conditions from Update are satisfied.
								_jump = false;
						}


						/*if (_vertical > 0)
		{
			_animator.SetInteger("dir", 2);
		}
		else if (_vertical < 0)
		{
			_animator.SetInteger("dir", 0);
		}
		else */
				if (_horizontal > 0) {
						if (!_facingRight) {
								_flip = true;
								Flip ();
								Vector3 gunSpriteScale = gunSprite.transform.localScale;
								gunSpriteScale.x *= -1;
								gunSprite.transform.localScale = gunSpriteScale;
								
								gunSprite.transform.position = new Vector3 (_groundCheck.transform.position.x+1.25f, _groundCheck.transform.position.y + 4.2f, 0);


						}
						rigidbody2D.AddForce (Vector2.right * _horizontal * 36f);	
						_animator.SetInteger ("dir", 1);
						_gun.transform.position = new Vector3 (_groundCheck.transform.position.x + 2.77f, _groundCheck.transform.position.y + 4.2f, 0);
						

						//vai pra direita
				} else if (_horizontal < 0) {
						if (_facingRight) {
								Flip ();
								_flip = true;
								Vector3 gunSpriteScale = gunSprite.transform.localScale;
								gunSpriteScale.x *= -1;
								gunSprite.transform.localScale = gunSpriteScale;

								gunSprite.transform.position = new Vector3 (_groundCheck.transform.position.x-0.12f, _groundCheck.transform.position.y + 4.3f, 0);
						}
						_direction.x = -0.1f;
						_animator.SetInteger ("dir", 3);
						_gun.transform.position = new Vector3 (_groundCheck.transform.position.x - 2.5f, _groundCheck.transform.position.y + 4.2f, 0);
						
						//vai pra esquerda
				} else {
						_animator.SetInteger ("dir", 4);
						if (_flip) {
								//Flip();
						}

				}
				_direction.y = _yVelocity;
	}
}
	void Flip ()
	{
		_facingRight = !_facingRight;
		_flip = false;
		/*_flip = false;
		// Switch the way the player is labelled as facing.
		_facingRight = !_facingRight;
		
		// Multiply the player's x local scale by -1.
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;*/
	}
}