using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Gun : MonoBehaviour
{
	public Rigidbody2D rocket;				// Prefab of the rocket.
	public Rigidbody2D bullet;				// Prefab of the rocket.
	public float speed = 20f;				// The speed the rocket will fire at.
	public bool shooting = false;
	public int count = 0;
	public AudioClip[] sounds;
	private float lastShootTime;


	private playerController playerController;		// Reference to the PlayerControl script.
	private Animator anim;					// Reference to the Animator component.

	private SpriteRenderer bomb1;			// Reference to the sprite renderer of the bomb
	private SpriteRenderer bomb2;			// Reference to the sprite renderer of the bomb
	private float bombTime;
	public Rigidbody2D bomba;				// Prefab of the rocket.
	public Text labelBomba;


	void Awake()
	{
		// Setting up the references.
		anim = transform.root.gameObject.GetComponent<Animator>();
		playerController = transform.root.GetComponent<playerController>();
		bomb1 = GameObject.Find("lancaBomba").GetComponent<SpriteRenderer>();
		bomb2 = GameObject.Find("lancaBomba2").GetComponent<SpriteRenderer>();
		labelBomba = GameObject.Find("BombLabel").GetComponent<Text>();
	}


	void Update ()
	{
		if (Time.time > bombTime + 0.5 && (bomb1.enabled || bomb2.enabled)) {

			playerController.enabled = true;
			if(bomb1.enabled)
			{
				bomb1.enabled = false;

				Rigidbody2D bombInstance = Instantiate (bomba, new Vector3 (transform.position.x+1, transform.position.y, 0), Quaternion.Euler (new Vector3 (0, 0, 0))) as Rigidbody2D;
				bombInstance.velocity = new Vector2 (10f, 0);
				playerController.bombsNumber--;
				labelBomba.text = (int.Parse(labelBomba.text)-1).ToString();
			}
			else{
				bomb2.enabled = false;
				Rigidbody2D bombInstance = Instantiate(bomba, transform.position, Quaternion.Euler(new Vector3(0,0,180f))) as Rigidbody2D;
				bombInstance.velocity = new Vector2(-10f, 0);
				playerController.bombsNumber--;
				labelBomba.text = (int.Parse(labelBomba.text)-1).ToString();
			}
			playerController.gunSprite.enabled = true;
			
		}
		
		if (Input.GetButtonDown ("Fire2")) {
			if(playerController.bombsNumber>0)
			{
				//_animator.SetBool ("bomb", true);
				playerController.enabled = false;
				if(playerController._facingRight)
				{
					bomb1.enabled = true;
					bomb2.enabled = false;
				}
				else{
					bomb2.enabled = true;
					bomb1.enabled = false;
				}
				playerController.gunSprite.enabled = false;
				bombTime = Time.time;
			}

		}

		if (Input.GetButtonUp ("Fire1")) {
			shooting = false;
				}

		// If the fire button is pressed and the gun is ak-47 fire
		if (shooting)
		{
			if(Time.time > lastShootTime + 0.1) {
				// ... instantiate the rocket facing right and set it's velocity to the right. 
				AudioSource.PlayClipAtPoint(sounds[1],transform.position);
				if(playerController._facingRight)
				{
					Rigidbody2D bulletInstance = Instantiate (bullet,  new Vector3(transform.position.x+3.5f, transform.position.y-0.1f, 0), Quaternion.Euler (new Vector3 (0, 0, 0))) as Rigidbody2D;
					bulletInstance.velocity = new Vector2 (speed, 0);
				}
				else{
					Rigidbody2D bulletInstance = Instantiate(bullet,  new Vector3(transform.position.x-2.5f, transform.position.y-0.1f, 0), Quaternion.Euler(new Vector3(0,0,180f))) as Rigidbody2D;
						bulletInstance.velocity = new Vector2(-speed, 0);
				}
				lastShootTime = Time.time;
				return;
			
			}
		}
		// If the fire button is pressed...
		if(Input.GetButtonDown("Fire1"))
		{
			// ... set the animator Shoot trigger parameter and play the audioclip.
			anim.SetTrigger("Shoot");
			//audio.Play();

			// If the player is facing right...
			if(playerController._facingRight)
			{
				if(playerController.gunNumber == 0 && Time.time > lastShootTime + 0.2)
				{
					lastShootTime = Time.time;
					// ... instantiate the rocket facing right and set it's velocity to the right. 
					AudioSource.PlayClipAtPoint(sounds[0],transform.position);
					Rigidbody2D bulletInstance = Instantiate(bullet, new Vector3(transform.position.x+3.5f, transform.position.y, 0), Quaternion.Euler(new Vector3(0,0,0))) as Rigidbody2D;
					bulletInstance.velocity = new Vector2(speed, 0);

				}
				else if(playerController.gunNumber == 1)
				{
					AudioSource.PlayClipAtPoint(sounds[1],transform.position);
					// ... instantiate the rocket facing right and set it's velocity to the right. 
					Rigidbody2D bulletInstance = Instantiate(bullet,  new Vector3(transform.position.x+3.5f, transform.position.y-0.1f, 0), Quaternion.Euler(new Vector3(0,0,0))) as Rigidbody2D;
					bulletInstance.velocity = new Vector2(speed, 0);
					shooting = true;
					lastShootTime = Time.time;
				}
				else if(playerController.gunNumber == 2){
					if(Time.time > lastShootTime + 1)
					{
						AudioSource.PlayClipAtPoint(sounds[2],transform.position);
						// ... instantiate the rocket facing right and set it's velocity to the right. 
						Rigidbody2D bulletInstance = Instantiate(rocket,  new Vector3(transform.position.x+0.5f, transform.position.y+0.25f, 0), Quaternion.Euler(new Vector3(0,0,0))) as Rigidbody2D;
						bulletInstance.velocity = new Vector2(speed, -0.6f);
						lastShootTime = Time.time;
					}

				}
			}
			else
			{
				if(playerController.gunNumber == 0 && Time.time > lastShootTime + 0.2)
				{
					lastShootTime = Time.time;
					// ... instantiate the rocket facing right and set it's velocity to the right. 
					AudioSource.PlayClipAtPoint(sounds[0],transform.position);
					Rigidbody2D bulletInstance = Instantiate(bullet,  new Vector3(transform.position.x-2.5f, transform.position.y, 0), Quaternion.Euler(new Vector3(0,0,180f))) as Rigidbody2D;
					bulletInstance.velocity = new Vector2(-speed, 0);
					
				}
				else if(playerController.gunNumber == 1)
				{
					AudioSource.PlayClipAtPoint(sounds[1],transform.position);
					// ... instantiate the rocket facing right and set it's velocity to the right. 
					Rigidbody2D bulletInstance = Instantiate(bullet,  new Vector3(transform.position.x-2.5f, transform.position.y, 0), Quaternion.Euler(new Vector3(0,0,180f))) as Rigidbody2D;
					bulletInstance.velocity = new Vector2(-speed, 0);
					shooting = true;
					lastShootTime = Time.time;
				}
				else if(playerController.gunNumber == 2){
					if(Time.time > lastShootTime + 1)
					{
						AudioSource.PlayClipAtPoint(sounds[2],transform.position);
						// ... instantiate the rocket facing right and set it's velocity to the right. 
						Rigidbody2D bulletInstance = Instantiate(rocket,  new Vector3(transform.position.x-0.2f, transform.position.y+0.3f, 0), Quaternion.Euler(new Vector3(0,0,180f))) as Rigidbody2D;
						bulletInstance.velocity = new Vector2(-speed, -0.6f);
						lastShootTime = Time.time;
					}
					
				}
				// Otherwise instantiate the rocket facing left and set it's velocity to the left.

			}
		}
	}
}
