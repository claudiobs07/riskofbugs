﻿using UnityEngine;
using System.Collections;

public class bullet : MonoBehaviour 
{
	public GameObject explosion;		// Prefab of explosion effect.
	
	//public playerController playerController;		// Reference to the PlayerControl script.

	void Start () 
	{
		// Destroy the rocket after 2 seconds if it doesn't get destroyed before then.
		Destroy(gameObject, 1);
		//playerController = transform.root.GetComponent<playerController>();
	}
	
	
	void OnExplode()
	{
		// Create a quaternion with a random rotation in the z-axis.
		Quaternion randomRotation = Quaternion.Euler(0f, 0f, Random.Range(0f, 360f));
		
		// Instantiate the explosion where the rocket is with the random rotation.
		Instantiate(explosion, transform.position, randomRotation);
	}
	
	void OnTriggerEnter2D (Collider2D col) 
	{
		// If it hits an enemy...
		if(col.tag == "Enemy")
		{
			// ... find the Enemy script and call the Hurt function.

			if(GameObject.Find("stand").GetComponent<playerController>().gunNumber==0)
			{
				col.gameObject.GetComponent<Enemy>().Hurt(1);
			}
			else
			{
				col.gameObject.GetComponent<Enemy>().Hurt(2);
			}
			
			// Call the explosion instantiation.
			OnExplode();
			
			// Destroy the rocket.
			Destroy (gameObject);
		}
		if(col.tag == "Spider")
		{
			// ... find the Enemy script and call the Hurt function.
			
			if(GameObject.Find("stand").GetComponent<playerController>().gunNumber==0)
			{
				col.gameObject.GetComponent<Enemy_spider>().Hurt(1);
			}
			else
			{
				col.gameObject.GetComponent<Enemy_spider>().Hurt(2);
			}
			
			// Call the explosion instantiation.
			OnExplode();
			
			// Destroy the rocket.
			Destroy (gameObject);
		}
		if(col.tag == "worm")
		{
			// ... find the Enemy script and call the Hurt function.
			
			if(GameObject.Find("stand").GetComponent<playerController>().gunNumber==0)
			{
				col.gameObject.GetComponent<Enemy_Worm>().Hurt(1);
			}
			else
			{
				col.gameObject.GetComponent<Enemy_Worm>().Hurt(2);
			}
			
			// Call the explosion instantiation.
			OnExplode();
			
			// Destroy the rocket.
			Destroy (gameObject);
		}

		// Otherwise if the player manages to shoot himself...
		else if(col.gameObject.tag != "Player")
		{
			// Instantiate the explosion and destroy the rocket.
			OnExplode();
			Destroy (gameObject);
		}
	}
}
