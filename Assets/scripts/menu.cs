﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class menu : MonoBehaviour {

	public float tempo;

	private playerController playerController;		// Reference to the PlayerControl script.
	private Gun gun;		// Reference to the PlayerControl script.

	public Canvas menuCanvas;
	public Canvas menuDeath;
	public Canvas menuGameOver;
	public Image livesImg;
	public Sprite [] livesImgs;
	// Use this for initialization
	void Start () {

		tempo = Time.timeScale;
		livesImg = GameObject.Find ("LivesImage").GetComponent<Image> ();
		menuCanvas = GameObject.Find ("MenuCanvas").GetComponent<Canvas>();
		menuDeath = GameObject.Find ("MenuDeath").GetComponent<Canvas>();
		menuGameOver = GameObject.Find ("GameOver").GetComponent <Canvas> ();
		playerController = GameObject.Find ("stand").GetComponent<playerController>();
		gun = GameObject.Find ("Gun").GetComponent<Gun>();
	}

	void Update(){
		if (Input.GetKeyDown(KeyCode.Escape)) {
			menuGUI();
				}
		}

	public void menuGUI(){
		Time.timeScale = (tempo+1f)%2f;
		tempo = Time.timeScale;
		menuCanvas.enabled = !menuCanvas.enabled;
		playerController.enabled = !playerController.enabled;
		gun.enabled = !gun.enabled;

	}
	public void goBackMenuButtonCLick(){
		Time.timeScale = (tempo+1f)%2f;
		tempo = Time.timeScale;
		menuCanvas.enabled = !menuCanvas.enabled;
		playerController.enabled = !playerController.enabled;
		gun.enabled = !gun.enabled;
		PlayerPrefs.DeleteAll ();
		Application.LoadLevel ("Menu");
	}
	public void ContinueButtonCLick(){
		Time.timeScale = 1;
		tempo = 1;
		menuCanvas.enabled = false;
		playerController.enabled = true;
		gun.enabled = true;
		
	}
	public void MenuDeath(){

		playerController.enabled = !playerController.enabled;
		gun.enabled = !gun.enabled;
		menuDeath.enabled = true;

		
	}
	public void ContinuarDeath(){
		menuDeath.enabled = false;

		//playerController.enabled = true;
		//gun.enabled = true;
		PlayerPrefs.SetFloat("hp", 100f);
		GameObject.Find ("stand").GetComponent<PlayerHealth>().turnAlive();
		GameObject.Find ("stand").GetComponent<PlatformGenerator> ().restarGame ();

		playerController.lives--;
		livesImg.sprite = livesImgs[playerController.lives];
	}
	public void goBackDeath(){
		menuDeath.enabled = !menuDeath.enabled;
		playerController.enabled = !playerController.enabled;
		gun.enabled = !gun.enabled;
		PlayerPrefs.DeleteAll ();
		Application.LoadLevel ("Menu");
		
	}
	public void gameOver(){
		
		playerController.enabled = !playerController.enabled;
		gun.enabled = !gun.enabled;
		menuGameOver.enabled = true;
		PlayerPrefs.DeleteAll ();
	}
}
