﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour
{	
	public float health = 100f;					// The player's health.
	public float repeatDamagePeriod = 2f;		// How frequently the player can be damaged.
	public AudioClip[] ouchClips;				// Array of clips to play when the player is damaged.
	public float hurtForce = 100f;				// The force with which the player is pushed when hurt.
	//public float damageAmount = 10f;			// The amount of damage to take when enemies touch the player

	private SpriteRenderer healthBar;			// Reference to the sprite renderer of the health bar.
	private float lastHitTime;					// The time at which the player was last hit.
	private Vector3 healthScale;				// The local scale of the health bar initially (with full health).
	private playerController playerController;		// Reference to the PlayerControl script.
	private Animator anim;						// Reference to the Animator on the player


	//C:\Users\thiago.donizetti\Downloads\docs\uf\2015\IPJ\RoB 1.21\RoB 1.21\Assets\Audio
	//http://www.player2.se/bilder/sst2dsample.gif
	//http://i59.tinypic.com/vhvkgj.png
	//http://www.feplanet.net/media/sprites/8/battle/sheets/enemy/monster_bael_fangs.gif


	void Awake ()
	{
		/*if (PlayerPrefs.HasKey ("hp")) {
			health = PlayerPrefs.GetFloat("hp");
			
		}*/
		// Setting up references.
		playerController = transform.root.GetComponent<playerController>();
		healthBar = GameObject.Find("bar").GetComponent<SpriteRenderer>();
		anim = GetComponent<Animator>();

		// Getting the intial scale of the healthbar (whilst the player has full health).
		healthScale = healthBar.transform.localScale;
	}


	void OnCollisionEnter2D (Collision2D col)
	{
		// If the colliding gameobject is an Enemy...
		if(col.gameObject.tag == "Enemy")
		{
			// ... and if the time exceeds the time of the last hit plus the time between hits...
			if (Time.time > lastHitTime + repeatDamagePeriod) 
			{
				// ... and if the player still has health...
				if(health > 0f)
				{
					// ... take damage and reset the lastHitTime.
					playerController.damageTime = Time.time;
					//playerController._moveForce=-1f;
					anim.SetInteger("dir", 4);
					TakeDamage(col.transform, 10f); 
					lastHitTime = Time.time; 
				}
				// If the player doesn't have health, do some stuff, let him fall into the river to reload the level.
				else
				{
					// Find all of the colliders on the gameobject and set them all to be triggers.
//					Collider2D[] cols = GetComponents<Collider2D>();
/*					foreach(Collider2D c in cols)
					{
						//c.transform.position.Set(0,0,0);
						//c.isTrigger = true;
					}*/

					// Move all sprite parts of the player to the front
//					SpriteRenderer[] spr = GetComponentsInChildren<SpriteRenderer>();
/*					foreach(SpriteRenderer s in spr)
					{
						//s.sortingLayerName = "UI";
					}*/

					// ... disable user Player Control script
					GetComponent<playerController>().enabled = false;

					// ... disable the Gun script to stop a dead guy shooting a nonexistant bazooka
					GetComponentInChildren<Gun>().enabled = false;

					// ... Trigger the 'Die' animation state
					Vector3 hurtVector = transform.position - col.transform.position + Vector3.up * 5f;
					rigidbody2D.AddForce(new Vector2(hurtVector.x*500f, 1000f));
					anim.SetTrigger("Die");
					GameObject.Find("colider_stand").collider2D.enabled = false;
					GameObject.Find("colider_dead").collider2D.enabled = true;
					GameObject.Find ("Canvas").GetComponent<menu>().MenuDeath();
				//	transform.localScale.Set(1f,1f,1f);

				}
			}
		}
		if(col.gameObject.tag == "worm")
		{
			// ... and if the time exceeds the time of the last hit plus the time between hits...
			if (Time.time > lastHitTime + repeatDamagePeriod) 
			{
				// ... and if the player still has health...
				if(health > 0f)
				{
					// ... take damage and reset the lastHitTime.
					playerController.damageTime = Time.time;
					//playerController._moveForce=-1f;
					anim.SetInteger("dir", 4);
					TakeDamage(col.transform, 20f); 
					lastHitTime = Time.time; 
				}
				// If the player doesn't have health, do some stuff, let him fall into the river to reload the level.
				else
				{
					// Find all of the colliders on the gameobject and set them all to be triggers.
					//					Collider2D[] cols = GetComponents<Collider2D>();
					/*					foreach(Collider2D c in cols)
					{
						//c.transform.position.Set(0,0,0);
						//c.isTrigger = true;
					}*/
					
					// Move all sprite parts of the player to the front
					//					SpriteRenderer[] spr = GetComponentsInChildren<SpriteRenderer>();
					/*					foreach(SpriteRenderer s in spr)
					{
						//s.sortingLayerName = "UI";
					}*/
					
					// ... disable user Player Control script
					GetComponent<playerController>().enabled = false;
					
					// ... disable the Gun script to stop a dead guy shooting a nonexistant bazooka
					GetComponentInChildren<Gun>().enabled = false;
					
					// ... Trigger the 'Die' animation state
					Vector3 hurtVector = transform.position - col.transform.position + Vector3.up * 5f;
					rigidbody2D.AddForce(new Vector2(hurtVector.x*500f, 1000f));
					anim.SetTrigger("Die");
					GameObject.Find("colider_stand").collider2D.enabled = false;
					GameObject.Find("colider_dead").collider2D.enabled = true;
					GameObject.Find ("Canvas").GetComponent<menu>().MenuDeath();
					//	transform.localScale.Set(1f,1f,1f);
					
				}
			}
		}
		// If the colliding gameobject is an Enemy...
		if(col.gameObject.tag == "Spider")
		{
			// ... and if the time exceeds the time of the last hit plus the time between hits...
			if (Time.time > lastHitTime + repeatDamagePeriod) 
			{
				// ... and if the player still has health...
				if(health > 1f)
				{
					// ... take damage and reset the lastHitTime.
					playerController.damageTime = Time.time;
					//playerController._moveForce=-1f;
					anim.SetInteger("dir", 4);

					//Chama Damage Duas vezes por que o dano e maior para a aranha
					TakeDamage(col.transform, 30f); 
					lastHitTime = Time.time; 
				}
				// If the player doesn't have health, do some stuff, let him fall into the river to reload the level.
				else
				{
					// Find all of the colliders on the gameobject and set them all to be triggers.
					//					Collider2D[] cols = GetComponents<Collider2D>();
					/*					foreach(Collider2D c in cols)
					{
						//c.transform.position.Set(0,0,0);
						//c.isTrigger = true;
					}*/
					
					// Move all sprite parts of the player to the front
					//					SpriteRenderer[] spr = GetComponentsInChildren<SpriteRenderer>();
					/*					foreach(SpriteRenderer s in spr)
					{
						//s.sortingLayerName = "UI";
					}*/
					
					// ... disable user Player Control script
					GetComponent<playerController>().enabled = false;
					
					// ... disable the Gun script to stop a dead guy shooting a nonexistant bazooka
					GetComponentInChildren<Gun>().enabled = false;
					
					// ... Trigger the 'Die' animation state
					Vector3 hurtVector = transform.position - col.transform.position + Vector3.up * 5f;
					rigidbody2D.AddForce(new Vector2(hurtVector.x*500f, 1000f));
					anim.SetTrigger("Die");
					GameObject.Find("colider_stand").collider2D.enabled = false;
					GameObject.Find("colider_dead").collider2D.enabled = true;
					GameObject.Find ("Canvas").GetComponent<menu>().MenuDeath();
					//	transform.localScale.Set(1f,1f,1f);
					
				}
			}
		}
	}


	void TakeDamage (Transform enemy, float damageAmount)
	{
		// Make sure the player can't jump.
		playerController._jump = false;

		// Create a vector that's from the enemy to the player with an upwards boost.
		Vector3 hurtVector = transform.position - enemy.position + Vector3.up * 5f;

		// Add a force to the player in the direction of the vector and multiply by the hurtForce.
	//	rigidbody2D.AddForce(Vector2.right, Vector2.right*-30f * playerController._moveForce);
		//rigidbody2D.AddForce(hurtVector * hurtForce);
		rigidbody2D.AddForce(new Vector2(hurtVector.x*800f, 1000f));
		//anim.SetInteger("dir", 4);
		// Reduce the player's health by 10.
		health -= damageAmount;

		// Update what the health bar looks like.
		UpdateHealthBar();

		// Play a random clip of the player getting hurt.
		int i = Random.Range (0, ouchClips.Length);
		AudioSource.PlayClipAtPoint(ouchClips[i], transform.position);
	//	PlayerPrefs.SetFloat("hp", health);
	}


	public void UpdateHealthBar ()
	{
		// Set the health bar's colour to proportion of the way between green and red based on the player's health.
		healthBar.material.color = Color.Lerp(Color.green, Color.red, 1 - health * 0.01f);

		// Set the scale of the health bar to be proportional to the player's health.
		healthBar.transform.localScale = new Vector3(healthScale.x * health * 0.01f, 1, 1);
	}

	public void turnAlive(){
		transform.position = new Vector3 (22.29f, 3.32f, 0);
		anim.SetInteger ("dir", 4);
		anim.SetBool ("Die", false);
		GameObject.Find("colider_stand").collider2D.enabled = true;
		GameObject.Find("colider_dead").collider2D.enabled = false;
		health = 100f + playerController.hpPoints*10;
		// Set the health bar's colour to proportion of the way between green and red based on the player's health.
		healthBar.material.color = Color.Lerp(Color.green, Color.red, 1 - health * 0.01f);
		
		// Set the scale of the health bar to be proportional to the player's health.
		healthBar.transform.localScale = new Vector3(healthScale.x * health * 0.01f, 1, 1);
		playerController.enabled = true;
		GameObject.Find ("Gun").GetComponent<Gun>().enabled = true;
		}
	public float getHealth(){
		return health;
		}


}
