﻿using UnityEngine;
using System.Collections;

public class bomba : MonoBehaviour 
{
	public GameObject explosion;		// Prefab of explosion effect.

	
	//public playerController playerController;		// Reference to the PlayerControl script.
	
	void Start () 
	{
		// Destroy the rocket after 2 seconds if it doesn't get destroyed before then.
		Destroy(gameObject, 2);
		//playerController = transform.root.GetComponent<playerController>();
	}
	
	
	void OnExplode()
	{
		// Create a quaternion with a random rotation in the z-axis.
	
		
		// Instantiate the explosion where the rocket is with the random rotation.
		Rigidbody2D ex = Instantiate(explosion, transform.position, Quaternion.Euler(new Vector3(0,0,0))) as Rigidbody2D;
		Destroy (ex, 2);
		Destroy (gameObject);
	}
	
	void OnTriggerEnter2D (Collider2D col) 
	{
		// If it hits an enemy...
		if(col.tag == "Enemy")
		{
			// ... find the Enemy script and call the Hurt function.

				col.gameObject.GetComponent<Enemy>().Hurt(10);
			
			// Call the explosion instantiation.
			OnExplode();
			
			// Destroy the rocket.
			Destroy (gameObject);
		}
		else if(col.tag == "Spider")
		{
			// ... find the Enemy script and call the Hurt function.

				col.gameObject.GetComponent<Enemy_spider>().Hurt(10);
			
			// Call the explosion instantiation.
			OnExplode();
			
			// Destroy the rocket.
			Destroy (gameObject);
		}else if(col.tag == "worm")
		{
			// ... find the Enemy script and call the Hurt function.
			
			col.gameObject.GetComponent<Enemy_Worm>().Hurt(10);
			
			// Call the explosion instantiation.
			OnExplode();
			
			// Destroy the rocket.
			Destroy (gameObject);
		}
		else if(col.tag == "Obstacle")
		{
			return;
		}		
		// Otherwise if the player manages to shoot himself...
		else if(col.gameObject.tag != "Player")
		{
			// Instantiate the explosion and destroy the rocket.
			OnExplode();
			Destroy (gameObject);
		}
	}
}
